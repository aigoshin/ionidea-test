<%@include file="/WEB-INF/jsp/init.jsp" %>
<html>
<head>
    <title>CDs</title>
</head>
<body>
<div class="site-wrapper">
    <h2 align="center">Catalog</h2>

    <table class="table table-hover">
        <tr>
            <th>Title</th>
            <th>Artist</th>
            <th>Country</th>
            <th>Company</th>
            <th>Price</th>
            <th>Year</th>
        </tr>

        <c:forEach var="cd" items="${data.currentPageList}">
            <tr>
                <td>${cd.title}</td>
                <td>${cd.artist}</td>
                <td>${cd.country}</td>
                <td>${cd.company}</td>
                <td>${cd.price}</td>
                <td>${cd.year}</td>
            </tr>
        </c:forEach>
    </table>

    <div align="center">
        <ul class="pagination pagination">
            <c:if test="${data.page >1 }">
                <li><a href="<c:url value="/cd/data?page=${data.page - 1}"/>">Previous</a></li>
            </c:if>
            <c:forEach begin="1" end="${data.maxPages}" var="i">
                <c:choose>
                    <c:when test="${data.page eq i}">
                        <li class="disabled"><a href="#">${i}</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="<c:url value="/cd/data/?page=${i}"/>">${i}</a></li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:if test="${data.page lt data.maxPages}">
                <li><a href="<c:url value="/cd/data?page=${data.page + 1}"/>">Next</a></li>
            </c:if>
        </ul>
    </div>

    <a class="btn btn-primary" href="<c:url value="/cd/downloadPage"/>">Download page</a>
    <a class="btn btn-primary" style="float: right" href="<c:url value="/cd/uploadPage"/>">Upload page</a>
</div>

</body>
</html>

