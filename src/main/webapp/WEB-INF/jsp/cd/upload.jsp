<%@include file="/WEB-INF/jsp/init.jsp" %>
<html>
<head>
    <title>Upload</title>
</head>
<body>
<div class="site-wrapper">
    <h2 align="center">Upload file</h2>

    <div align="center">
        <form:form role="form" cssClass="form-inline" modelAttribute="uploadedFile" action="/cd/upload" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <form:input path="file" id="file" type="file" name="file" cssClass="btn btn-default"/>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
            </div>
            <div><form:errors path="file" cssClass="error"/></div>
            <div class="error">${error}</div>
        </form:form>
    </div>
</div>
</body>
</html>
