package com.ionidea.validation;


import com.ionidea.models.UploadedFile;
import net.sf.oval.constraint.CheckWithCheck;

public class CheckFile implements CheckWithCheck.SimpleCheck {

    public boolean isSatisfied(Object validatedObject, Object value) {
        return ((UploadedFile) validatedObject).getFile().getOriginalFilename().endsWith(".xml");
    }
}