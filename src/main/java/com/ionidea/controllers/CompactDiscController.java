package com.ionidea.controllers;


import com.ionidea.models.UploadedFile;
import com.ionidea.services.CompactDiscService;
import com.ionidea.utils.Pageable;
import net.sf.oval.integration.spring.SpringValidator;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.*;


@Controller
@RequestMapping("/cd")
public class CompactDiscController {

    @Autowired
    private SpringValidator validator;

    @Autowired
    private CompactDiscService compactDiscService;

    @RequestMapping("/data")
    public String getData(Model model, @RequestParam(value = "page", defaultValue = "1", required = false) int page) throws FileNotFoundException, JAXBException {
        Pageable pageable = new Pageable(compactDiscService.getAllCDs(), page, 5);
        model.addAttribute("data", pageable);
        return "cd/show";
    }

    @RequestMapping(value = "/uploadPage", method = RequestMethod.GET)
    public String uploadPage(Model model) {
        model.addAttribute("uploadedFile", new UploadedFile());
        return "cd/upload";
    }

    @RequestMapping(value = "/downloadPage", method = RequestMethod.GET)
    public String downloadPage() {
        return "cd/download";
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String upload(@ModelAttribute("uploadedFile") UploadedFile file, BindingResult result, Model model) throws IOException {

        validator.validate(file, result);
        if (result.hasErrors()) {
            return "cd/upload";
        }

        try {
            compactDiscService.addCDs(file);
        } catch (JAXBException e) {
            model.addAttribute("error", "Wrong data format");
            return "cd/upload";
        }
        return "redirect:/cd/data";

    }

    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public void download(HttpServletResponse response) throws IOException {
        IOUtils.copy(compactDiscService.getInputStreamCatalogXml(), response.getOutputStream());
        response.setContentType("text/xml");
        response.setHeader("Content-Disposition", "attachment; filename=catalog.xml");
        response.flushBuffer();
    }

}
