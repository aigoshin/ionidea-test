package com.ionidea.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "CATALOG")
public class Catalog {

    private List<CompactDisc> cdList;

    @XmlElement(name = "CD")
    public List<CompactDisc> getCdList() {
        return cdList;
    }

    public void setCdList(List<CompactDisc> cdList) {
        this.cdList = cdList;
    }


}
