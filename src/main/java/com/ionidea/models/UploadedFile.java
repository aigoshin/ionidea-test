package com.ionidea.models;


import com.ionidea.validation.CheckFile;
import net.sf.oval.constraint.CheckWith;
import org.springframework.web.multipart.MultipartFile;

public class UploadedFile {

    @CheckWith(value = CheckFile.class, message = "Wrong file. Please select .xml file")
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
