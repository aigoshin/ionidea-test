package com.ionidea.services;


import com.ionidea.models.CompactDisc;
import com.ionidea.models.UploadedFile;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface CompactDiscService {

    public void addCDs(UploadedFile uploadedFile) throws IOException, JAXBException;

    public List<CompactDisc> getAllCDs() throws FileNotFoundException, JAXBException;

    InputStream getInputStreamCatalogXml() throws FileNotFoundException;
}

