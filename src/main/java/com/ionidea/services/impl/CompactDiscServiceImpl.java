package com.ionidea.services.impl;


import com.ionidea.models.Catalog;
import com.ionidea.models.CompactDisc;
import com.ionidea.models.UploadedFile;
import com.ionidea.services.CompactDiscService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class CompactDiscServiceImpl implements CompactDiscService, InitializingBean {

    private static final String CATALOG_XML = "./catalog.xml";

    private Marshaller marshaller;
    private Unmarshaller unmarshaller;

    @Override
    public void afterPropertiesSet() throws Exception {
        JAXBContext context = JAXBContext.newInstance(Catalog.class);
        marshaller = context.createMarshaller();
        unmarshaller = context.createUnmarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    }

    @Override
    public void addCDs(UploadedFile uploadedFile) throws IOException, JAXBException {
        Catalog catalog = (Catalog) unmarshaller.unmarshal(uploadedFile.getFile().getInputStream());

        List<CompactDisc> existingCompactDiscs = getAllCDs();
        List<CompactDisc> newCompactDiscs = catalog.getCdList();

        for (CompactDisc compactDisc : newCompactDiscs) {
            if (!existingCompactDiscs.contains(compactDisc)) {
                existingCompactDiscs.add(bindNewCompactDisc(compactDisc));
            } else {
                existingCompactDiscs.set(existingCompactDiscs.indexOf(compactDisc), compactDisc);
            }
        }

        catalog.setCdList(existingCompactDiscs);
        marshaller.marshal(catalog, new File(CATALOG_XML));
    }

    @Override
    public List<CompactDisc> getAllCDs() {
        Catalog catalog = null;
        try {
            catalog = (Catalog) unmarshaller.unmarshal(new FileReader(CATALOG_XML));
        } catch (JAXBException e) {
            return new ArrayList<>();
        } catch (FileNotFoundException e) {
            return new ArrayList<>();
        }
        return catalog.getCdList();
    }

    @Override
    public InputStream getInputStreamCatalogXml() throws FileNotFoundException {
        return new FileInputStream(new File(CATALOG_XML));
    }

    private CompactDisc bindNewCompactDisc(CompactDisc compactDisc) {
        CompactDisc CD = new CompactDisc();
        CD.setTitle(compactDisc.getTitle());
        CD.setArtist(compactDisc.getArtist());
        CD.setCompany(compactDisc.getCompany());
        CD.setCountry(compactDisc.getCountry());
        CD.setPrice(compactDisc.getPrice());
        CD.setYear(compactDisc.getYear());
        return compactDisc;
    }
}
